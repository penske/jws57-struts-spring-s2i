# Tomcat 9 S2i


## Description
The following guide is used to create a deployment in a Red Hat Openshfit cluster. The guide will have instrictons on how to build your image and ultimately deploy it. 

1. [Build](#build-image)
2. [Create Database Secret](#create-database-secret)
3. [Deploy](#deploy-image) 
4. [Route & Service](#route-and-service-creation) 

### **Prequisite**
*  Make sure you are logged in, on your CLI in order to run these commands.

# Build Image
The provided Build config can be configured to point to any source repository. W will also create an Image stream first, that will point to the internal image registry with Openshift. 

### **Prequisite**
*  It is currenty pointing to jboss quickstart project that connects to a database. Modify this with your source or continue using this project for your deployment.

    `uri: URI to your repository`

    `ref: Branch in your repository`

    `(Optional) context-dir: Directory within the repository that will be our source `

    ```
        git:
          ref: master
          uri: 'https://github.com/jboss-openshift/openshift-quickstarts.git'
        context-dir: 'tomcat-jdbc'
    ```

### Commands

1. Create Image Stream
```
oc apply -f image-stream.yaml
```
2. Create Build Config
```
oc apply -f build-config.yaml
```


# Create Database Secret
The tomcat image requires Environment variables containing the database details in order to generate a context.xml for applications to be able to use as a data source. 

The file db-secret.yaml needs to be modified before the following secret can be created. The following are the values that need to be created.

The most important value is `DB_SERVICE_PREFIX_MAPPING`. From this env value, it will dictate how other values will be read by the tomcat base image. This value is equal to `<POOL-NAME>-postgresql=<PREFIX>`, where `<POOL-NAME>` is the name of your connection pool and `<PREFIX>` is the name of the prefix that will come before each of your environment variables. 

Example: 

* If our value is the following &rarr;

    `DB_SERVICE_PREFIX_MAPPING: "test-postgresql=DB"`

    * *The values before the `=` can only be lowercase.*

    Then the rest of our values will look like this &rarr;
    ```
    DB_JNDI: "java:jboss/datasources/test-postgresql"
    DB_USERNAME: "postgres"
    DB_PASSWORD: "postgres"
    DB_DATABASE: "database"
    DB_DRIVER: "postgresql"
    DB_POSTGRESQL_SERVICE_HOST: "localhost"
    DB_POSTGRESQL_SERVICE_PORT: '5432'
    DB_MAX_POOL_SIZE: '10'
    DB_MIN_POOL_SIZE: '5'
    DB_URL: 'jdbc:postgresql://localhost:5432/database'
    DB_NONXA: 'true'
    TEST_POSTGRESQL_SERVICE_HOST: 'localhost'
    TEST_POSTGRESQL_SERVICE_PORT: '5432'
    ```

### Commands

1. Create Database Secret
```
oc apply -f db-secret.yaml
```


# Deploy Image

Next we will deploy the image we just created. After the image is built, the image will pushed to the image stream "tomcat-9-s2i". Before deploying the image make sure database secret has been created.

### Commands



1. Create Deployment
```
oc apply -f deployment.yaml
```

Our deployment should have the status of running

![Pod is running on Openshift](images/tomcatRunning.png)


# Route and Service Creation
Next we will create the service for our pod and also expose this service with a route that we will be able to reach from our browser.


1. Create Service
```
oc apply -f service.yaml
```

2. Create Route
```
oc apply -f route.yaml
```

You can find the URL to your app in the `Routes` section of Networking

![Find routes in the Route section](images/tomcatRoute.png)